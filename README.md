# Geekstuff.dev / Devcontainers / Features / Kubectl

This devcontainer feature installs Kubectl, and its autocomplete.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/kubectl": {}
    }
}
```

You can use a `debian`, `ubuntu` or `alpine` image as the base.

This feature requires the basics feature which adds a `dev` non-root user
and here we mount an isolated and dedicated docker volume for the `~/.kube` folder.

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/kubectl/-/tags).
